<?php


namespace Drupal\tt_video_field\Twig;

class VideoFieldExtension extends \Twig_Extension
{


    public function getName()
    {
        return 'tt_video_field';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('tt_video_field_format', [
                $this,
                'videoFieldFormat'
            ], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * @param $data
     * @return array|bool
     */
    public function videoFieldFormat($data)
    {
        if (!$data || empty($data)) {
            return false;
        }

        $formatted = [];
        foreach ($data as $row) {
            if (isset($row->width)) {
                $formatted[] = array(
                    'size'   => (string)$row->width,
                    'source' => [ // Why is this a array with a object FE?
                        (object)[
                            'url'  => $row->link,
                            'type' => $row->type
                        ]
                    ]
                );
            }
        }

        return $formatted;
    }
}
